/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int centered = 0;                    /* -c  option; centers dmenu on screen */
static int min_width = 500;                 /* minimum width when centered */
static int colorprompt = 1;					/* -p  option; if 1, prompt uses SchemeSel, otherwise SchemeNorm */
static const int user_bh = 0;               /* add an defined amount of pixels to the bar height */
/* -fn option overrides fonts[0]; default X11 font or font set */
static char *fonts[] = {
	"monospace:size=10",
	"NotoColorEmoji:pixelsize=8:antialias=true:autohint=true"
};
static const unsigned int bgalpha = 0xe0;
static const unsigned int fgalpha = OPAQUE;
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#bbbbbb", "#222222" },
    [SchemeNormHighlight] = { "#ffc978", "#222222" },
	[SchemeSel] = { "#eeeeee", "#005577" },
    [SchemeSelHighlight] = { "#ffc978", "#005577" },
	[SchemeOut] = { "#000000", "#00ffff" },
    [SchemeOutHighlight] = { "#ffc978", "#00ffff" },
	[SchemeCursor] = { "#222222", "#bbbbbb"},
};
static const unsigned int alphas[SchemeLast][2] = {
	/*		fgalpha		bgalphga	*/
	[SchemeNorm] = { fgalpha, bgalpha },
	[SchemeNormHighlight] = { fgalpha, bgalpha },
	[SchemeSel] = { fgalpha, bgalpha },
	[SchemeSelHighlight] = { fgalpha, bgalpha },
	[SchemeOut] = { fgalpha, bgalpha },
	[SchemeOutHighlight] = { fgalpha, bgalpha },
    [SchemeCursor] = { fgalpha, bgalpha },
};

/* -l and -g options; controls number of lines and columns in grid if > 0 */
static unsigned int lines      = 0;
static unsigned int columns    = 0;
/* -h option; minimum height of a menu line */
static unsigned int lineheight = 0;
static unsigned int min_lineheight = 8;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 0;

/* -n option; preselected item starting from 0; */
static unsigned int preselected = 0;

/*
 * Use prefix matching by default; can be inverted with the -x flag.
 */
static int use_prefix = 0;

/*
 * -vi option; if nonzero, vi mode is always enabled and can be
 * accessed with the global_esc keysym + mod mask
 */
static unsigned int vi_mode = 1;
static unsigned int start_mode = 0;			/* mode to use when -vi is passed. 0 = insert mode, 1 = normal mode */
static Key global_esc = { XK_n, Mod1Mask };	/* escape key when vi mode is not enabled explicitly */
static Key quit_keys[] = {
	/* keysym	modifier */
	{ XK_q,		0 }
};
